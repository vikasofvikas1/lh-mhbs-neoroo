import 'package:flutter/material.dart';

class Dimensions {
  BuildContext context;
  Dimensions(this.context);

  double get width => MediaQuery.of(context).size.width;  // width of the screen
  double get height => MediaQuery.of(context).size.height;  // height of the screen

  double get horizontalPaddingHomeScreen => MediaQuery.of(context).size.width/45;
  double get verticalPaddingHomeScreen => MediaQuery.of(context).size.height/40;

}