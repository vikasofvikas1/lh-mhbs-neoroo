import 'package:flutter/material.dart';

final mainColor = Color(0xff6E78F7);
final textColor = Color(0xff3B3C3F);

final backgroundNotificationColor = Color(0xFFFFBB1B); // background color of notification
final backgroundColor = Color(0xFFF5F5F5); //background color of home screen

final temperatureColor = Color(0xff09AF6D);

final breathingRateColor = Color(0xFFFF421D);

final heartRateColor = Color(0xFF097DAF);
