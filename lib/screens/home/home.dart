import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';
import 'package:neoroo_app/screens/home/components/custom_app_bar.dart';
import 'package:neoroo_app/screens/home/components/aboutUs_tile.dart';
import 'package:neoroo_app/screens/home/components/new_training_module_list.dart';
import 'package:neoroo_app/screens/home/components/skin_to_skin_row.dart';
import 'package:neoroo_app/screens/home/components/vitals_row.dart';
import 'package:neoroo_app/screens/home/components/baby_selection.dart';
import 'package:neoroo_app/config/text_style.dart';
import 'package:neoroo_app/config/dimensions.dart';
import '../../utils/constants.dart';

//Home Screen

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}
class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: backgroundColor,
          body: ListView(
            children: [

              CustomAppBar(),  // app bar

              SizedBox(height: Dimensions(context).verticalPaddingHomeScreen),

              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2*Dimensions(context).horizontalPaddingHomeScreen),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "About us",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, fontFamily: openSans,),
                    ),
                    SizedBox(height:  Dimensions(context).verticalPaddingHomeScreen),

                    AboutUsTile(),

                    SizedBox(height: Dimensions(context).verticalPaddingHomeScreen),

                    BabySelection(),

                    Text("Vitals", style: notificationNumber),
                    SizedBox(height:  Dimensions(context).verticalPaddingHomeScreen),
                    VitalsRow(),

                    SizedBox(height:  Dimensions(context).verticalPaddingHomeScreen),

                    Text("Skin-to-Skin", style: notificationNumber),
                    SizedBox(height:  Dimensions(context).verticalPaddingHomeScreen/2),

                    SkinToSkinRow(),

                    ///training modules
                    SizedBox(height:  Dimensions(context).verticalPaddingHomeScreen),

                    Text("New Training Modules", style: notificationNumber),
                    SizedBox(height:  Dimensions(context).verticalPaddingHomeScreen/2),

                    NewTrainingModuleList(),

                  ],
                ),
              )
            ],
          )),
    );
  }
}


