import 'package:flutter/material.dart';
import 'package:neoroo_app/config/dimensions.dart';

import '../../../utils/constants.dart';

class TrainingModulesLayout extends StatefulWidget {
  const TrainingModulesLayout({
    super.key,
    required this.title,
    required this.details,
    required this.image,
  });
  final String title;
  final String details;
  final Image image;

  @override
  State<TrainingModulesLayout> createState() => _TrainingModulesLayoutState();
}

class _TrainingModulesLayoutState extends State<TrainingModulesLayout> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Dimensions(context).height/7,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.white,

      ),
      child: Row(
        children: [
          // video play button
          Stack(
            alignment: Alignment.center,
            children: [
              SizedBox(
                height: Dimensions(context).height/7,
                width:  Dimensions(context).height/7,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: widget.image,
                ),
              ),

                CircleAvatar(
                    radius: 25,
                    backgroundColor: Colors.black.withOpacity(0.5),
                    child: Icon(
                      Icons.play_arrow_rounded,
                      size: 50,
                      color: Colors.white,
                    )),

            ],
          ),

          // information
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        widget.title,
                        style: TextStyle(fontWeight: FontWeight.bold, fontFamily: openSans,),
                        maxLines: 1,
                      ),
                      Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: Colors.grey,
                      )
                    ],
                  ),
                  const SizedBox(height: 2),
                  Expanded(
                      child: Text(
                        widget.details,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontFamily: openSans,),
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Icon(Icons.save_alt),
                      const SizedBox(width: 10),

                      //bookmark
                      Icon(Icons.bookmark, color: Colors.grey),
                      const SizedBox(width: 10),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}