import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';

import '../../../utils/constants.dart';

class BabySelection extends StatelessWidget {
  const BabySelection({super.key});

  @override
  Widget build(BuildContext context) {
    return

      Row(
        children: [
          const SizedBox(height: 26),
          Text(
            "Baby Mila",
            style: TextStyle(
                color: mainColor,
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: openSans,),
          ),
          Spacer(),
          Container(
            height: 34,
            padding: EdgeInsets.symmetric(horizontal: 18),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(18)),
            child: DropdownButtonHideUnderline(
                child: DropdownButton<String?>(
                    hint: Padding(
                      padding:
                      const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        "Select Baby", style: TextStyle(color: Colors.grey.withOpacity(0.7), fontFamily: openSans,),
                      ),
                    ),
                    elevation: 0,
                    onChanged: (String? name) { },

                    items: [
                      DropdownMenuItem(
                        child: Row(
                            children: [
                          Radio(
                            value: "Mila",
                            onChanged: (baby1) { print("press"); },
                            groupValue: "Mila",
                            activeColor: mainColor,
                          ),
                          Text("Baby Mila", style: TextStyle(fontStyle: FontStyle.italic, fontFamily: openSans,),)
                        ]),
                        value: "Mila",
                      ),

                      DropdownMenuItem(
                        child: Row(children: [
                          Radio(
                            value: "Joe",
                            onChanged: (baby2) { print("press");},
                            groupValue: "Joe1",
                          ),
                          Text("Baby Joe", style: TextStyle(fontStyle: FontStyle.italic, fontFamily: openSans,),)
                        ]),
                        value: "Joe",
                      ),

                      DropdownMenuItem(
                        child: Row(children: [
                          Radio(
                            value: "Henry",
                            onChanged: (baby3) { print("press");},
                            groupValue: "Henry2",
                          ),
                          Text("Baby Henry", style: TextStyle(fontStyle: FontStyle.italic, fontFamily: openSans,),)
                        ]),
                        value: "Henry",
                      )




                    ])),

                  )
                ],
              );
          }
        }