import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';
import 'package:neoroo_app/config/dimensions.dart';

import '../../../utils/constants.dart';

class STSWidget extends StatelessWidget {
  const STSWidget(
      {super.key,
        required this.icon,
        required this.title,
        required this.timeValue});

        final IconData icon;
        final String title;
        final String timeValue;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Dimensions(context).height/13,
      width: Dimensions(context).width/2.3,
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 3),
                blurRadius: 2.5,
                spreadRadius: 1,
                color: Colors.grey.withOpacity(0.5))
          ]),
      child: Row(
        children: [
          Container(
            height: Dimensions(context).height/18,
            width:  Dimensions(context).height/18,
            decoration: BoxDecoration(
                color: mainColor, borderRadius: BorderRadius.circular(12)),
            child: Icon(
              icon,
              size: 28,
              color: Colors.white,
            ),
          ),
          const SizedBox(width: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(color: textColor, fontFamily: openSans,),
              ),
              Text(
                timeValue,
                style: TextStyle(color: textColor, fontWeight: FontWeight.w700, fontFamily: openSans,),
              )
            ],
          )
        ],
      ),
    );
  }
}