import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';
import 'package:neoroo_app/config/dimensions.dart';

import '../../../utils/constants.dart';

class AboutUsTile extends StatelessWidget {
  const AboutUsTile({super.key});
  @override
  Widget build(BuildContext context) {
    return

      Container(
        height: Dimensions(context).height/2,
        width: double.infinity,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 10,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
          borderRadius: BorderRadius.circular(25),
          color: Colors.white
        ),

        child: Column(
          children: [
            Expanded(
              flex: 10,
              child: SizedBox.expand(
                child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                    child: Image.network(
                      "https://images.pexels.com/photos/3875083/pexels-photo-3875083.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                      fit: BoxFit.cover,
                    )),
              ),
            ),
            Expanded(
              flex: 7,
              child: Padding(
                padding:
                EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: Text(
                  "NeoRoo is an application connected to sensor-enabled swaddling device which helps to support the adoption and uptake skin-to-skin care among adult-baby dyadsand automatically collects key vital signs information from newborns (for example, heart rate, breathing rate  and pattern, oxygen saturation), within the context of the United States health care system.",
                  style: TextStyle(color: textColor, fontSize: 14, fontFamily: openSans,),
                ),
              ),
            ),
          ],
        ),
      );

  }
}