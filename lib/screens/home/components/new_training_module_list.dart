import 'package:flutter/material.dart';
import 'package:neoroo_app/screens/home/home.dart';
import 'package:neoroo_app/screens/home/components/training_module_widget.dart';


class NewTrainingModuleList extends StatelessWidget {
  const NewTrainingModuleList({super.key});

  @override
  Widget build(BuildContext context) {
    return

      Column(
        children: [
          TrainingModulesLayout(
            title: "Carrying your baby skin to skin",
            details:
            "NeoRoo is an application connected to sensor-enabled swaddling device.",
            image: Image.network(
                "https://images.pexels.com/photos/4668995/pexels-photo-4668995.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                fit: BoxFit.cover),
          ),
          const SizedBox(height: 16),
          TrainingModulesLayout(
            title: "Carrying your baby skin to skin",
            details:
            "NeoRoo is an application connected to sensor-enabled swaddling device.",
            image: Image.network(
                "https://images.pexels.com/photos/1648374/pexels-photo-1648374.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
                fit: BoxFit.cover),
          ),
          const SizedBox(height: 16),

        ],
      );
  }
}