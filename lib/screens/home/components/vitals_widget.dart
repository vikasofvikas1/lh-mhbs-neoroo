import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';
import 'package:neoroo_app/config/dimensions.dart';

import '../../../utils/constants.dart';

class VitalsWidget extends StatelessWidget {

  final IconData icon;
  final String title;
  final String temperatureValue;
  final Color valueColor;

  const VitalsWidget({
    super.key,
    required this.title,
    required this.temperatureValue,
    required this.valueColor,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: Dimensions(context).height/10,
          width: Dimensions(context).width/3.5,
          margin: const EdgeInsets.only(top: 22),

          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    spreadRadius: 1,
                    blurRadius: 3,
                    color: Colors.grey.withOpacity(0.5))
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(height: 20),
              Text(
                title,
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14, fontFamily: openSans,),
              ),
              Text(
                temperatureValue,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    color: valueColor,
                    fontSize: 14,
                    fontFamily: openSans,),
              ),
            ],
          ),
        ),

        // The circle icon
        Positioned(
            top: 0,
            child: CircleAvatar(
              radius: 20,
             backgroundColor: mainColor,
              child: ImageIcon(
                AssetImage("assets/icon_images/icon_lungs.png"),
                color: Color(0xFF3A5A98),
              ),
            ))
      ],
    );
  }
}
