import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';
import 'package:neoroo_app/config/dimensions.dart';

import '../../../utils/constants.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({super.key});  //to enable Flutter to identify and track the widget in the widget tree

  @override
  Widget build(BuildContext context) {

    return AppBar(
      backgroundColor: mainColor,
      toolbarHeight: Dimensions(context).height/7,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      title: Padding(
          padding: EdgeInsets.only(left:  Dimensions(context).horizontalPaddingHomeScreen),
          child: Text("Welcome back!", style: TextStyle(fontSize: 22, fontFamily: openSans,))),
      actions: [
        Stack(
          alignment: Alignment.center,
          children: [
            IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.notifications_sharp,
                  size: Dimensions(context).width/15,
                )),
            Positioned(
              top: 50,
              right: 10,
              child: CircleAvatar(
                radius: Dimensions(context).width/60,
                backgroundColor: backgroundNotificationColor,
                child: Text(
                  "1",
                 style: TextStyle(fontSize: 12,color: Colors.white, fontFamily: openSans,),
                ),
              ),
            )
          ],
        ),
         SizedBox(width: Dimensions(context).horizontalPaddingHomeScreen)
      ],
    );
  }
}