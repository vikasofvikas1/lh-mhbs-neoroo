import 'package:flutter/material.dart';
import 'package:neoroo_app/config/color_palette.dart';
import 'package:neoroo_app/screens/home/home.dart';
import 'package:neoroo_app/screens/home/components/vitals_widget.dart';

class VitalsRow extends StatelessWidget {
  const VitalsRow({super.key});

  @override
  Widget build(BuildContext context) {
    return
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                VitalsWidget(title: "Temperature", temperatureValue: "97.0 °F", icon: Icons.thermostat_rounded, valueColor: temperatureColor,),
                VitalsWidget(title: "Breathing Rate", temperatureValue: "97.0 °F", icon: Icons.air, valueColor:breathingRateColor),
                VitalsWidget(title: "Heart Rate", temperatureValue: "97.0 °F", icon: Icons.favorite_rounded, valueColor:heartRateColor)
              ],
            );
          }
        }