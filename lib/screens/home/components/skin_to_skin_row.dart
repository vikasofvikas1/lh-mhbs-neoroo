import 'package:flutter/material.dart';
import 'package:neoroo_app/screens/home/home.dart';
import 'package:neoroo_app/screens/home/components/sts_widget.dart';

class SkinToSkinRow extends StatelessWidget {
  const SkinToSkinRow({super.key});

  @override
  Widget build(BuildContext context) {
    return

      Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            STSWidget(
              icon: Icons.access_time_outlined,
              title: "STS Time",
              timeValue: "20.09 min",
            ),

            STSWidget(
              icon: Icons.calendar_month_outlined,
              title: "STS this week",
              timeValue: "30.00 hr",
            ),

          ]);
  }
}